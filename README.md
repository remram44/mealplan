MealPlan
========

This is a web application allowing users to create and share recipes, plan their meals, and generate a shopping list.

It will also allow you to update your inventory (checking off what you have) to take into account when shopping.

It is built in Python using Django.

Quick set-up guide to hack on MealPlan
--------------------------------------

You need Python 3 and Django. You can set all this up using [Pipenv](https://docs.pipenv.org/en/latest/):

    $ pipenv install

Activate the virtual environment using::

    $ pipenv shell

Create the configuration, database, admin user, and load example data::

    $ cp mealplan/settings.dist.py mealplan/settings.py
    $ ./manage.py migrate
    $ ./manage.py createsuperuser
    $ ./manage.py loaddata recipes/fixtures/sample.json

This creates the `db.sqlite3` file. You can then run the server using::

    $ ./manage.py runserver

Browse to [`http://127.0.0.1:8000/`](http://127.0.0.1:8000/).

Database organization
---------------------

![table diagram](models.png)

Deploying MealPlan
------------------

This can be deployed on a web server like other WSGI applications. See [the Django documentation](https://docs.djangoproject.com/en/2.2/howto/deployment/) for more details.
