import datetime

from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.http import HttpResponseForbidden, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from recipes.models import Recipe
from .models import Plan, PlannedMeal, PlannedDish

MEAL_ORDER = [
    'breakfast',
    'brunch',
    'lunch',
    'coffee',
    'tea',
    'aperitif',
    'dinner',
]
MEAL_ORDER = {n: i for i, n in enumerate(MEAL_ORDER)}


@login_required
def edit_plan(request, plan_id=None):
    TODO  # Create or rename plan


def parse_date_or_404(date):
    try:
        if not date:
            raise ValueError
        if len(date) != 10 or date[4] != '-' or date[7] != '-':
            raise ValueError
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[8:10])
        return datetime.date(year, month, day)
    except ValueError:
        return HttpResponseNotFound()


@login_required
def plan(request, plan_id=None, date=None):
    plan = get_object_or_404(Plan, pk=plan_id)
    if plan.owner != request.user:
        return HttpResponseForbidden(
            "You don't have access to this recipe."
        )
    if date is None:
        # Last Monday
        dt = timezone.now()
        dt = dt - datetime.timedelta(days=dt.weekday())
        return redirect('plan_date', plan_id, dt.strftime('%Y-%m-%d'))

    date = parse_date_or_404(date)

    # Get all the meals for that week
    meals = plan.plannedmeal_set.filter(
        date__gte=date,
        date__lt=date + datetime.timedelta(days=7),
    )

    # Organize them by day
    days = [
        dict(
            date=(date + datetime.timedelta(days=i)).strftime('%Y-%m-%d'),
            meals=[],
        )
        for i in range(7)
    ]
    for meal in meals:
        day = (meal.date - date).days
        days[day]['meals'].append(meal)

    # Order them by meal name
    for day in days:
        day['meals'].sort(key=lambda n: MEAL_ORDER.get(n, 9999))

    return render(
        request, 'plan.html',
        {
            'plan': plan, 'date': date, 'days': days,
            'prev_week': date - datetime.timedelta(days=7),
            'next_week': date + datetime.timedelta(days=7),
        },
    )


class PlannedMealForm(ModelForm):
    class Meta:
        model = PlannedMeal
        fields = ['meal_name', 'comment']


@login_required
def planned_meal(request, plan_id, planned_meal_id=None, date=None):
    plan = get_object_or_404(Plan, pk=plan_id)
    if plan.owner != request.user:
        return HttpResponseForbidden(
            "You don't have access to this meal plan."
        )

    if request.method == 'POST':
        if planned_meal_id is not None:
            meal = get_object_or_404(PlannedMeal, pk=planned_meal_id)
            if meal.plan != plan:
                return HttpResponseNotFound()
            new = False
        else:
            meal = None
            new = True
            date = parse_date_or_404(date)
        form = PlannedMealForm(request.POST, instance=meal)
        if form.is_valid():
            meal = form.save(commit=False)
            meal.plan = plan
            if date is not None:
                meal.date = date
            meal.save()
            return redirect('meal', plan.id, meal.id)
        else:
            return render(request, 'meal.html', {'form': form, 'new': new})
    else:
        if planned_meal_id is None:
            form = PlannedMealForm()
            return render(request, 'meal.html', {'form': form, 'new': True})
        else:
            meal = get_object_or_404(PlannedMeal, pk=planned_meal_id)
            if meal.plan != plan:
                return HttpResponseNotFound()
            form = PlannedMealForm(instance=meal)
            return render(
                request, 'meal.html',
                {
                    'form': form, 'new': False,
                    'plan': plan, 'meal': meal,
                    'dishes': meal.planneddish_set.all(),
                    'recipes': Recipe.objects.all(),
                },
            )


@login_required
def planned_meal_dishes(request, plan_id, planned_meal_id):
    plan = get_object_or_404(Plan, pk=plan_id)
    if plan.owner != request.user:
        return HttpResponseForbidden(
            "You don't have access to this meal plan."
        )
    meal = get_object_or_404(PlannedMeal, pk=planned_meal_id)
    if meal.plan != plan:
        return HttpResponseNotFound()
    for dish in meal.planneddish_set.all():
        try:
            quantity = float(request.POST['quantity_%d' % dish.id])
        except (ValueError, KeyError):
            pass
        else:
            if quantity == 0:
                dish.delete()
            else:
                dish.quantity = quantity
                dish.save()
    return redirect(
        'meal',
        plan_id=plan_id, planned_meal_id=planned_meal_id,
    )


@login_required
def planned_meal_add_dish(request, plan_id, planned_meal_id):
    plan = get_object_or_404(Plan, pk=plan_id)
    if plan.owner != request.user:
        return HttpResponseForbidden(
            "You don't have access to this meal plan."
        )
    meal = get_object_or_404(PlannedMeal, pk=planned_meal_id)
    if meal.plan != plan:
        return HttpResponseNotFound()
    recipe = get_object_or_404(Recipe, pk=request.POST['recipe'])
    if recipe.owner != request.user:
        return HttpResponseForbidden()
    PlannedDish(
        meal=meal,
        # FIXME: Person?
        # FIXME: Comment?
        quantity=float(request.POST['quantity']),
        recipe=recipe,
    ).save()
    return redirect(
        'meal',
        plan_id=plan_id, planned_meal_id=planned_meal_id,
    )


@login_required
def shopping_list(request, plan_id):
    TODO  # TODO: View a shopping list from plan
    # Can be compared with an inventory
    # Should take a limit duration (don't consider meal that are x days away)
    # Should show which ingredients are available only from specific stores
