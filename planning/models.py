from django.contrib.auth.models import User
from django.db import models

import recipes.models


class Plan(models.Model):
    """A user can have multiple meal plans, example home/family, etc.

    It is where the planned meals are recorded.
    """
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class PlannedMeal(models.Model):
    """Planned meal, which comprises multiple dishes.

    The meal is identified by the day and a "meal name" (e.g. "lunch",
    "dinner", "snack", "aperitif"). It also has a "person" field in case
    different people or groups will eat different things.

    It links to a recipe, and includes the quantity of the recipe that is
    required, to multiply the required ingredients.
    """
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    date = models.DateField(null=False)
    meal_name = models.CharField(max_length=200, blank=True)
    comment = models.TextField(blank=True)


class PlannedDish(models.Model):
    """A dish is a single recipe realized as part of a meal.

    It is a certain quantity of a recipe. It also has a "person" field in case
    different people or groups will eat different things in a meal.
    """
    meal = models.ForeignKey(PlannedMeal, on_delete=models.CASCADE)
    person = models.CharField(max_length=200, blank=True)
    comment = models.TextField(blank=True)
    quantity = models.FloatField()
    recipe = models.ForeignKey(recipes.models.Recipe, on_delete=models.PROTECT)
