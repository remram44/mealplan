from django.contrib import admin

from . import models


admin.site.register(models.Inventory)
admin.site.register(models.OwnedIngredient)
admin.site.register(models.Store)
admin.site.register(models.IngredientInStore)
