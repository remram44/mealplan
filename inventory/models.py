from django.contrib.auth.models import User
from django.db import models

from recipes.models import Ingredient


class Inventory(models.Model):
    """A user can have multiple inventories, example home/office/cabin.

    It is where the owned ingredients are recorded.
    """
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "inventories"


class OwnedIngredient(models.Model):
    """An ingredient in the inventory.

    This records how much of a given ingredient is owned. When compared with
    a recipe or a meal plan, it is used to build a shopping list.

    It can be linked to stores if this ingredient can only be found in
    specific places, or it can be marked as "common".
    """
    inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    quantity = models.FloatField()
    common = models.BooleanField(null=False, default=True)
    comment = models.TextField(blank=True)

    def __str__(self):
        return "%s for %s" % (self.ingredient.name, self.inventory.name)


class Store(models.Model):
    """A store, e.g. a place where ingredients can be found.
    """
    inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class IngredientInStore(models.Model):
    """Records that an ingredient can be found in a specific store (or not).

    Can include comments, such as "usually" or "comes in on Tuesdays".
    """
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    present = models.BooleanField()
    comment = models.TextField(blank=True)

    def __str__(self):
        return "%s in %s" % (self.ingredient.name, self.store.name)
