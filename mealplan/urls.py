"""mealplan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.urls import path

import inventory.views
import planning.views
import recipes.views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', recipes.views.index, name='index'),
    path('login', LoginView.as_view(template_name='login.html'), name='login'),
    path('recipe/<int:recipe_id>', recipes.views.recipe, name='recipe'),
    path('recipe/new', recipes.views.recipe, name='new-recipe'),
    path(
        'recipe/<int:recipe_id>/ingredients',
        recipes.views.recipe_ingredients, name='recipe-ingredients',
    ),
    path(
        'recipe/<int:recipe_id>/add-ingredient',
        recipes.views.recipe_add_ingredient, name='recipe-add-ingredient',
    ),
    path('plan/<int:plan_id>', planning.views.plan, name='plan'),
    path(
        'plan/<int:plan_id>/date/<str:date>',
        planning.views.plan, name='plan_date',
    ),
    path('plan/<int:plan_id>/edit', planning.views.edit_plan, name='edit-plan'),
    path('plan/new', planning.views.edit_plan, name='new-plan'),
    path(
        'plan/<int:plan_id>/meal/<int:planned_meal_id>',
        planning.views.planned_meal, name='meal',
    ),
    path(
        'plan/<int:plan_id>/meal/new/<str:date>',
        planning.views.planned_meal, name='new-meal',
    ),
    path(
        '/plan/<int:plan_id>/meal/<int:planned_meal_id>/dishes',
        planning.views.planned_meal_dishes, name='meal_dishes',
    ),
    path(
        '/plan/<int:plan_id>/meal/<int:planned_meal_id>/add-dish',
        planning.views.planned_meal_add_dish, name='meal_add_dish',
    ),
    path(
        'plan/<int:plan_id>/shop',
        planning.views.shopping_list, name='plan_shop',
    ),
    path(
        'inventory/<int:inventory_id>',
        inventory.views.inventory, name='inventory',
    ),
    path(
        'inventory/<int:inventory_id>/store/<int:store_id>',
        inventory.views.store, name='store',
    ),
]
