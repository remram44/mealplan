from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Ingredient(models.Model):
    """An ingredient.

    This is global to the application, and ingredients shouldn't be deleted.
    They might get merged by admins.
    """
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    vegan = models.BooleanField(null=False, default=False)
    vegetarian = models.BooleanField(null=False, default=False)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    """A recipe.

    A recipe belongs to a user. It has instructions and ingredients.

    There is a parent relationship so that if a user import a recipe from
    another, she can be notified that a new version exist. Or they can know
    where they got it from.
    """
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    source = models.TextField(blank=True)
    cuisine = models.CharField(max_length=200)
    instructions = models.TextField()
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    parent = models.ForeignKey('Recipe', on_delete=models.SET_NULL, null=True,
                               blank=True)
    created = models.DateField(auto_now_add=True)
    last_modified = models.DateField(auto_now=True)

    def get_absolute_url(self):
        return reverse('recipe', args=[self.id])

    def __str__(self):
        return self.name


class RecipeIngredient(models.Model):
    """An ingredient in a recipe.

    It points to an Ingredient and include the quantity that the recipe needs,
    in kilograms.
    """
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    quantity = models.FloatField()

    def __str__(self):
        return "%s in %s" % (self.ingredient.name, self.recipe.name)
