from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render

from inventory.models import Inventory
from planning.models import Plan

from .models import Ingredient, Recipe, RecipeIngredient


def index(request):
    if not request.user.is_authenticated:
        return render(request, 'welcome.html')

    return render(
        request, 'dashboard.html',
        {
            'recipes': Recipe.objects.all(),
            'plans': Plan.objects.all(),
            'inventories': Inventory.objects.all(),
        }
    )


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ['name', 'description', 'instructions']


@login_required
def recipe(request, recipe_id=None):
    if request.method == 'POST':
        recipe = None
        new = True
        if recipe_id is not None:
            recipe = get_object_or_404(Recipe, pk=recipe_id)
            if recipe.owner != request.user:
                return HttpResponseForbidden(
                    "You don't have access to this recipe."
                )
            new = False
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            recipe = form.save(commit=False)
            recipe.owner = request.user
            recipe.save()
            return redirect('recipe', recipe_id=recipe.id)
        else:
            return render(request, 'recipe.html', {'form': form, 'new': new})
    else:
        if recipe_id is None:
            form = RecipeForm()
            return render(request, 'recipe.html', {'form': form, 'new': True})
        else:
            recipe = get_object_or_404(Recipe, pk=recipe_id)
            if recipe.owner != request.user:
                return HttpResponseForbidden(
                    "You don't have access to this recipe."
                )
            form = RecipeForm(instance=recipe)
            return render(
                request, 'recipe.html',
                {
                    'form': form, 'new': False,
                    'recipe_ingredients': recipe.recipeingredient_set.all(),
                    'ingredients': Ingredient.objects.all(),
                },
            )
    # TODO: Create an ingredient
    # Should also show a shopping list by comparing to an inventory


@login_required
def recipe_ingredients(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if recipe.owner != request.user:
        return HttpResponseForbidden("You don't have access to this recipe.")
    for ingredient in recipe.recipeingredient_set.all():
        try:
            quantity = float(request.POST['quantity_%d' % ingredient.id])
        except (ValueError, KeyError):
            pass
        else:
            if quantity == 0:
                ingredient.delete()
            else:
                ingredient.quantity = quantity
                ingredient.save()
    return redirect('recipe', recipe_id=recipe_id)


@login_required
def recipe_add_ingredient(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if recipe.owner != request.user:
        return HttpResponseForbidden("You don't have access to this recipe.")
    ingredient = get_object_or_404(Ingredient, pk=request.POST['ingredient'])
    RecipeIngredient(
        ingredient=ingredient,
        recipe=recipe,
        quantity=float(request.POST['quantity']),
    ).save()
    return redirect('recipe', recipe_id=recipe_id)
